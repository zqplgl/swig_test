/* File: example.i */
%module mathfunctions
%{
#define SWIG_FILE_WITH_INIT
#include "mathfunctions.h"
%}
void hello(int i);

%include "numpy.i"
%init %{
    import_array();
%}

%apply ( float* IN_ARRAY2, int DIM1, int DIM2 )
{(float* data, int w, int h)}

%inline %{
void test(float* data, int w, int h){
    test1(data, w, h);
}
%}