#include <iostream>

void test1(float* data, int w, int h){
    std::cout<<"wh: "<<w<<"\t"<<h<<std::endl;
    for(int i=0; i<h; ++i){
        float* tmp = data+w*i;
        for(int j=0; j<w; ++j){
            std::cout<<tmp[j]<<"\t";
        }
        std::cout<<std::endl;
    }
    return;
}

void hello(int i){
    std::cout<<"i: "<<i<<std::endl;
}